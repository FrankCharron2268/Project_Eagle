﻿using UnityEngine;

public class FireProjectile : MonoBehaviour
{
    public float fireRate = 1f;
    public float fireSpeed = 10f;
    public float lifeSpan = 2f;

    public GameObject projectile;
    public Transform fireTransform;
    public AudioSource audioSource;
    public AudioClip fireClip;
    public bool isPlayer = false;

    private string myTag;
    private float timeElapsedFire;

    private void Start()
    {
        myTag = transform.tag;
    }

    private void Update()
    {
        timeElapsedFire += Time.deltaTime;

        if (Input.GetMouseButtonDown(0) && isPlayer)
        {
            if (timeElapsedFire >= fireRate)
            {
                Shoot();
                timeElapsedFire = 0;
            }
        }
    }

    public void Shoot()
    {
        var temp = Instantiate(projectile, fireTransform.position, fireTransform.rotation);
        temp.GetComponent<Projectile>().myTag = myTag;
        temp.GetComponent<Rigidbody>().velocity = fireTransform.forward * fireSpeed;

        audioSource.clip = fireClip;
        audioSource.Play();
        // Destroy the bullet after 2 seconds
    }
}