﻿using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public AudioClip[] levelMusicChangeArray;

    private AudioSource audioSource;

    // Use this for initialization
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        Debug.Log("Dont destroy on Load" + name);
    }

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnLevelWasLoaded(int level)//Deprecated...OnEnable() mais jpas sur encore
    {
        AudioClip thisLevelMusic = levelMusicChangeArray[level];
        Debug.Log("Playing Clip" + thisLevelMusic);
        if (thisLevelMusic) //if theres some music attached
        {
            audioSource.clip = thisLevelMusic;
            audioSource.loop = true;
            audioSource.Play();
        }
    }
}