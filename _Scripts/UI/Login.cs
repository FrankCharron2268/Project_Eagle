﻿using System;
using System.Collections;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class Login : MonoBehaviour
{
    private string baseURL = "http://www.hgworks.online/ProjectEagle/";
    private string connectURL = "Game_Connect.php?";
    private string forgetURL = "IForgotMyAccount.php?";
    private string change_PWD_URL = "ChangePasswordAcct.php?";
    private string createURL = "CreatePlayerUser.php?";

    private int application_Reset_Counter = 0;

    private __GLOBALS__ glob;

    //Where we get the data
    public UnityEngine.UI.InputField Login_Email_Field;
    public UnityEngine.UI.InputField Login_Password_Field;

    public UnityEngine.UI.InputField PWD_Password_Field;
    public UnityEngine.UI.InputField PWD_Confirm_Password_Field;

    public UnityEngine.UI.InputField Create_User_Name_Field;
    public UnityEngine.UI.InputField Create_Email_Field;
    public UnityEngine.UI.InputField Create_Password_Field;
    public UnityEngine.UI.InputField Create_Password_Confirm_Field;

    public UnityEngine.UI.Toggle Remember_Toggle;

    //Here the Panels
    public GameObject Login_Panel;
    public GameObject PWD_Panel;
    public GameObject Create_Panel;

    //Text to display the result on
    public Text Login_Status_Text;
    public Text PWD_Status_Text;
    public Text Create_Status_Text;

    public LevelManager levelManager;

    private void Start()
    {
        try
        {           //La Classe                                 //Le Tag                    //La Classe jcomrpend pas
            glob = (__GLOBALS__)GameObject.FindGameObjectWithTag("__LE_TAG_GLOBAL__").GetComponent<__GLOBALS__>();
        }
        catch (NullReferenceException ex)
        {
            for (int i = 0; i < 42; i++)
            {
                Debug.Log("ERROR CODE : 42 : Start Game Testing From Intro Scene, Global Object not found. Loading Intro Scene.");
            }

            levelManager.LoadLevel("00 Splash");
            return;
        }

        //Reset Panels just in case we moved them in development.
        PWD_Panel.SetActive(false);
        Create_Panel.SetActive(false);

        Login_Panel.SetActive(true);

        if (this.Login_Password_Field != null)
        {
            Login_Password_Field.contentType = InputField.ContentType.Password;
        }

        if (this.PWD_Password_Field != null)
        {
            PWD_Password_Field.contentType = InputField.ContentType.Password;
        }

        if (this.PWD_Confirm_Password_Field != null)
        {
            PWD_Confirm_Password_Field.contentType = InputField.ContentType.Password;
        }

        if (glob.PLAYERINFO.REMEMBERINFO == 1)
        {
            Login_Password_Field.text = "Nice Try but Nope";
            Login_Email_Field.text = glob.PLAYERINFO.PLAYEREMAIL;

            this.Login_Password_Field.ForceLabelUpdate();
            this.Login_Email_Field.ForceLabelUpdate();

            Remember_Toggle.isOn = true;
        }
    }

    #region Login_Panel

    //Function to contact WebApp and Attempt a Login
    public void Login_Pressed()
    {
        if (Login_Email_Field.text.Equals("") || Login_Email_Field.text.Length < 8)
        {
            Login_Status_Text.text = "Enter your E-mail.";
            return;
        }

        if (Login_Password_Field.text.Equals(""))
        {
            Login_Status_Text.text = "Enter your Password.";
            return;
        }
        else if (Login_Password_Field.text.Length >= 4)
        {
            StartCoroutine(Connect(Login_Email_Field.text, Login_Password_Field.text, CONNECT_ARGUMENT.LOAD_MAIN_MENU));
        }
        else
        {
            Login_Status_Text.text = "Wrong E-mail/Password.";
            return;
        }
    }

    public void Forgot_Pressed()
    {
        if (Login_Email_Field.text.Equals(""))
        {
            Login_Status_Text.text = "Enter your E-mail.";
            return;
        }

        if (Login_Email_Field.text.Length <= 10)
        {
            Login_Status_Text.text = "Enter a valid E-mail.";
            return;
        }

        if (!Login_Email_Field.text.Contains("@"))
        {
            Login_Status_Text.text = "Enter a valid E-mail.";
            return;
        }

        if (application_Reset_Counter < 3)
        {
            StartCoroutine(Reset(Login_Email_Field.text));
            application_Reset_Counter++;
        }
        else
        {
            application_Reset_Counter = 42;
        }
    }

    public void New_Account_Pressed()
    {
        //Directly go to other Scene.
        Login_Email_Field.text = "";
        Login_Password_Field.text = "";

        Remember_Toggle.isOn = false;

        glob.PLAYERINFO.ResetData();

        Login_Panel.SetActive(false);

        Create_Panel.SetActive(true);
    }

    public void Change_Password_Pressed()
    {
        //Must Check Password/Login
        if (Login_Email_Field.text.Equals(""))
        {
            Login_Status_Text.text = "Enter your E-mail.";
            return;
        }

        if (Login_Password_Field.text.Equals(""))
        {
            Login_Status_Text.text = "Enter your Password.";
            return;
        }
        else if (Login_Password_Field.text.Length >= 4)
        {
            StartCoroutine(Connect(Login_Email_Field.text, Login_Password_Field.text, CONNECT_ARGUMENT.CHANGE_PASSWORD));
        }
        else
        {
            Login_Status_Text.text = "Wrong E-mail/Password.";
            return;
        }
    }

    public void Toggle_Remember()
    {
        if (!Remember_Toggle.isOn)
        {
            glob.PLAYERINFO.REMEMBERINFO = 0;
            glob.PLAYERINFO.PLAYERPASSWORDHASH = "";
            glob.PLAYERINFO.PLAYEREMAIL = "";

            Login_Email_Field.text = "";
            Login_Password_Field.text = "";
        }
    }

    public void Toggle_Password_Login()
    {
        if (this.Login_Password_Field != null)
        {
            if (this.Login_Password_Field.contentType == InputField.ContentType.Password)
            {
                this.Login_Password_Field.contentType = InputField.ContentType.Standard;
            }
            else
            {
                this.Login_Password_Field.contentType = InputField.ContentType.Password;
            }

            this.Login_Password_Field.ForceLabelUpdate();
        }
    }

    public void On_Password_Text_Edit()
    {
        Remember_Toggle.isOn = false;
        glob.PLAYERINFO.REMEMBERINFO = 0;
        glob.PLAYERINFO.PLAYERPASSWORDHASH = "";
        glob.PLAYERINFO.PLAYEREMAIL = "";
    }

    public void Toggle_Password_Change_Password()
    {
        if (this.PWD_Password_Field != null)
        {
            if (this.PWD_Password_Field.contentType == InputField.ContentType.Password)
            {
                this.PWD_Password_Field.contentType = InputField.ContentType.Standard;
                this.PWD_Confirm_Password_Field.contentType = InputField.ContentType.Standard;
            }
            else
            {
                this.PWD_Password_Field.contentType = InputField.ContentType.Password;
                this.PWD_Confirm_Password_Field.contentType = InputField.ContentType.Password;
            }

            this.PWD_Password_Field.ForceLabelUpdate();
            this.PWD_Confirm_Password_Field.ForceLabelUpdate();
        }
    }

    #endregion Login_Panel

    #region Create_Panel

    public void Confirm_Create_Pressed()
    {
        if (Create_User_Name_Field.text.Equals("") || Create_User_Name_Field.text.Length < 6 || Create_User_Name_Field.text.Length > 18)
        {
            Create_Status_Text.text = "Enter your User Name (6-18).";
            return;
        }

        if (Create_Email_Field.text.Equals("") || Create_Email_Field.text.Length < 8)
        {
            Create_Status_Text.text = "Enter your E-mail.";
            return;
        }

        if (Create_Password_Field.text.Equals("") || Create_Password_Field.text.Length < 6 || Create_Password_Field.text.Length > 18)
        {
            Create_Status_Text.text = "Enter a valid Password (6-18)";
            return;
        }

        if (Create_Password_Confirm_Field.text.Equals("") || Create_Password_Confirm_Field.text.Length < 6 || Create_Password_Confirm_Field.text.Length > 18)
        {
            Create_Status_Text.text = "Enter a valid Password (6-18)";
            return;
        }

        if (!Create_Password_Field.text.Equals(Create_Password_Confirm_Field.text))
        {
            Create_Status_Text.text = "Passwords Are Different";
            return;
        }

        //CreatePlayer
        StartCoroutine(CreatePlayer(Create_Email_Field.text, Create_User_Name_Field.text, Create_Password_Field.text));
    }

    public void Cancel_Create_Pressed()
    {
        Create_User_Name_Field.text = "";
        Create_Email_Field.text = "";
        Create_Password_Field.text = "";
        Create_Password_Confirm_Field.text = "";

        Create_Status_Text.text = "";

        Create_Panel.SetActive(false);

        Login_Panel.SetActive(true);
    }

    #endregion Create_Panel

    #region Password_Panel

    public void Confirm_Change_Password_Pressed()
    {
        if (PWD_Password_Field.text.Equals("") || PWD_Password_Field.text.Length < 6 || PWD_Password_Field.text.Length > 18)
        {
            PWD_Status_Text.text = "Enter a valid Password (6-18)";
            return;
        }

        if (PWD_Confirm_Password_Field.text.Equals("") || PWD_Confirm_Password_Field.text.Length < 6 || PWD_Confirm_Password_Field.text.Length > 18)
        {
            PWD_Status_Text.text = "Enter a valid Password (6-18)";
            return;
        }

        if (!PWD_Password_Field.text.Equals(PWD_Confirm_Password_Field.text))
        {
            PWD_Status_Text.text = "Passwords Are Different";
            return;
        }

        StartCoroutine(ChangePassword(glob.PLAYERINFO.PLAYEREMAIL, PWD_Confirm_Password_Field.text));
    }

    public void Cancel_Change_Password_Pressed()
    {
        PWD_Password_Field.text = "";
        PWD_Confirm_Password_Field.text = "";

        PWD_Panel.SetActive(false);

        Login_Panel.SetActive(true);
    }

    #endregion Password_Panel

    #region coroutines

    //COROUTINES

    // remember to use StartCoroutine when calling this function!
    private IEnumerator Connect(string email, string password, CONNECT_ARGUMENT argC)
    {
        if (Remember_Toggle.isOn)
        {
            if (glob.PLAYERINFO.PLAYERPASSWORDHASH == "")
            {
                password = hashString(password);
            }
            else
            {
                password = glob.PLAYERINFO.PLAYERPASSWORDHASH;
            }
        }
        else
        {
            password = hashString(password);
        }

        string post_url = baseURL + connectURL + "user_mail=" + WWW.EscapeURL(email) + "&user_password=" + password;

        // Post the URL to the site and create a download object to get the result.
        WWW hs_post = new WWW(post_url);
        yield return hs_post; // Wait until the download is done

        if (hs_post.error != null)
        {
            Debug.Log("Error Connecting!! ---:::::::--- " + hs_post.error);
        }
        else
        {
            if (hs_post.text.Contains("Welcome"))
            {
                Login_Status_Text.text = "Connected.";

                if (Remember_Toggle.isOn)
                {
                    glob.PLAYERINFO.REMEMBERINFO = 1;
                    glob.PLAYERINFO.PLAYERPASSWORDHASH = password;
                }

                glob.PLAYERINFO.PLAYEREMAIL = email;

                switch (argC)
                {
                    case CONNECT_ARGUMENT.LOAD_MAIN_MENU:
                        levelManager.LoadLevel("02a Main Menu");
                        break;

                    case CONNECT_ARGUMENT.CREATE_ACCOUNT:

                        break;

                    case CONNECT_ARGUMENT.CHANGE_PASSWORD:
                        Login_Password_Field.text = "";

                        Remember_Toggle.isOn = false;

                        Login_Panel.SetActive(false);

                        PWD_Panel.SetActive(true);
                        break;
                }
            }
            else if (hs_post.text.Contains("Account not Confirmed!"))
            {
                Login_Status_Text.text = "Account not Confirmed. Please Check Your E-Mails.";
            }
            else
            {
                Login_Status_Text.text = "Wrong E-mail/Password.";
            }
        }
    }

    // remember to use StartCoroutine when calling this function!
    private IEnumerator Reset(string email)
    {
        //This connects to a server side php script that will add the name and score to a MySQL DB.
        // Supply it with a string representing the players name and the players score.

        string post_url = baseURL + forgetURL + "user_mail=" + WWW.EscapeURL(email);

        // Post the URL to the site and create a download object to get the result.
        WWW hs_post = new WWW(post_url);
        yield return hs_post; // Wait until the download is done

        if (hs_post.error != null)
        {
            Login_Status_Text.text = "Check your internet connection.";

            Debug.Log("Error Connecting!! ---:::::::--- " + hs_post.error);
        }
        else
        {
            if (hs_post.text.Contains("Email Found"))
            {
                Login_Status_Text.text = "Password Reset. Check your E-mails.";
            }
            else if (hs_post.text.Contains("Account not Confirmed"))
            {
                Login_Status_Text.text = "Account not confirmed.";
            }
            else
            {
                Login_Status_Text.text = "Email Not Found.";
            }
        }
    }

    // remember to use StartCoroutine when calling this function!
    private IEnumerator ChangePassword(string email, string new_password)
    {
        //This connects to a server side php script that will add the name and score to a MySQL DB.
        // Supply it with a string representing the players name and the players score.

        new_password = hashString(new_password);

        string post_url = baseURL + change_PWD_URL + "user_mail=" + WWW.EscapeURL(email) + "&pass_hash=" + WWW.EscapeURL(new_password);

        // Post the URL to the site and create a download object to get the result.
        WWW hs_post = new WWW(post_url);
        yield return hs_post; // Wait until the download is done

        if (hs_post.error != null)
        {
            PWD_Status_Text.text = "Check your internet connection.";

            Debug.Log("Error Connecting!! ---:::::::--- " + hs_post.error);
        }
        else
        {
            if (hs_post.text.Contains("Success"))
            {
                PWD_Password_Field.text = "";
                PWD_Confirm_Password_Field.text = "";
                PWD_Status_Text.text = "Enter a new Password.";

                PWD_Panel.SetActive(false);

                Login_Panel.SetActive(true);
            }
            else
            {
                PWD_Status_Text.text = "Something Happenned.";
                Debug.Log(hs_post.text + "    " + hs_post.error);
            }
        }
    }

    private IEnumerator CreatePlayer(string email, string username, string password)
    {
        //This connects to a server side php script that will add the name and score to a MySQL DB.
        // Supply it with a string representing the players name and the players score.

        password = hashString(password);

        string post_url = baseURL + createURL + "user_mail=" + WWW.EscapeURL(email) + "&user_name=" + WWW.EscapeURL(username) + "&pass_hash=" + WWW.EscapeURL(password);

        // Post the URL to the site and create a download object to get the result.
        WWW hs_post = new WWW(post_url);
        yield return hs_post; // Wait until the download is done

        if (hs_post.error != null)
        {
            Create_Status_Text.text = "Check your internet connection.";

            Debug.Log("Error Connecting!! ---:::::::--- " + hs_post.error);
        }
        else
        {
            if (hs_post.text.Contains("Success"))
            {
                Create_User_Name_Field.text = "";
                Create_Email_Field.text = "";
                Create_Password_Field.text = "";
                Create_Password_Confirm_Field.text = "";

                Create_Status_Text.text = "";

                Create_Panel.SetActive(false);

                Login_Panel.SetActive(true);
            }
            else if (hs_post.text.Contains("Duplicate"))
            {
                Create_Status_Text.text = "Player Exists Already.";
            }
            else
            {
                Create_Status_Text.text = "Something Happenned.";
                Debug.Log(hs_post.text + "    " + hs_post.error);
            }
        }
    }

    #endregion coroutines

    #region utilities

    private string hashString(string value)
    {
        UTF8Encoding ue = new UTF8Encoding();
        byte[] bytes = ue.GetBytes(value);

        SHA256CryptoServiceProvider sha256 = new SHA256CryptoServiceProvider();
        byte[] hashBytes = sha256.ComputeHash(bytes);

        string hashString = "";

        for (int i = 0; i < hashBytes.Length; i++)
        {
            hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
        }

        return hashString.PadLeft(32, '0');
    }

    #endregion utilities
}

public enum CONNECT_ARGUMENT
{
    LOAD_MAIN_MENU = 0,
    CHANGE_PASSWORD = 1,
    CREATE_ACCOUNT = 2
}