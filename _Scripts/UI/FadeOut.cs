﻿using UnityEngine;
using UnityEngine.UI;

public class FadeOut : MonoBehaviour
{
    public float fadeInTime;
    private Image fadePanel;
    private Color currentColor = Color.black;

    // Use this for initialization
    private void Start()
    {
        fadePanel = GetComponent<Image>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (Time.timeSinceLevelLoad < fadeInTime)
        {
            //fade in
            float alphaChange = Time.deltaTime * fadeInTime;
            currentColor.a -= alphaChange;
            fadePanel.color = currentColor;
        }
        else
        {
            //deactivate panel
            gameObject.SetActive(false);
        }
    }
}