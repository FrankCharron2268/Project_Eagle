﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PanelManager : MonoBehaviour
{
    private string previousBtnSelection;
    private string previousBtnSelection2;
    private string currentSelection;

    private bool summaryIsActive;
    private bool lvlSelectIsActive;
    private bool isActive;
    private bool isActive2;
    [Header("--------------------PANELS-------------------")]
    [Space(10)]
    public GameObject Summary_Panel;
    public GameObject Level_Select_Panel;
    public GameObject[] panels;
    [Header("-------------------BUTTONS-------------------")]
    [Space(10)]
    public Button level_Select_Btn;
    public Button diff_Select_Btn;

    [Header("--------------------TEXTS--------------------")]
    public Text Level_Name_Text;
    public Text Level_Req_Text;
    public Text Level_Summary_Text;
    public Text diff_Name_Text;
    public Text selected_DiffName_Text;
    public Text diff_desc_Text;

    // Use this for initialization

    private void Start()
    {
        UIDictionary.Init();

        foreach (GameObject element in panels)
        {
            element.SetActive(false);
        }
        isActive = false;
        isActive2 = false;
    }

    public void ShowHidePanelLayer1(GameObject panel)
    {
        var go = EventSystem.current.currentSelectedGameObject;
        string c = (string)go.name;
        if (c != previousBtnSelection && isActive)
        {
            foreach (GameObject element in panels)
            {
                element.SetActive(false);
                isActive = false;
                isActive2 = false;
            }
            panel.SetActive(true);
            isActive = true;
        }
        else if (isActive && c == previousBtnSelection)
        {
            foreach (GameObject element in panels)
            {
                element.SetActive(false);
            }
            isActive = false;
            isActive2 = false;
        }
        else
        {
            panel.gameObject.SetActive(true);

            isActive = true;
        }
        previousBtnSelection = c;
    }

    public void ShowHideMultipleChoicePanel(GameObject panel)
    {
        var go = EventSystem.current.currentSelectedGameObject;
        string c = (string)go.name;
        //si is active2 = false ouvre le pannel et met isactive2 a true
        if (!isActive2)
        {
            panel.SetActive(true);
            isActive2 = true;
        }
        //si le bouton clicker est le meme que le precedent, ferme le panneau et isActive2 = false
        else if (c == previousBtnSelection2 && isActive2)
        {
            panel.SetActive(false);
            isActive2 = false;
        }
        else if (c == previousBtnSelection2 && !isActive2)
        {
            panel.SetActive(true);
            isActive2 = true;
        }
        //si cest un different fait rien, il va se remplir par lautre fonction
        previousBtnSelection2 = c;
    }

    //public void getCurrentSelection()
    //{
    //    var go = EventSystem.current.currentSelectedGameObject;
    //    currentSelection = (string)go.name;
    //    if (go != null)
    //    {
    //        Debug.Log("Clicked on : " + currentSelection);
    //    }
    //    else
    //    {
    //        Debug.Log("currentSelectedGameObject is null");
    //    }
    //}

    public void FillPanelSummary()
    {
        var go = EventSystem.current.currentSelectedGameObject;
        currentSelection = (string)go.name;
        string c = currentSelection;
        if (go != null)
        {
            Level_Name_Text.text = UIDictionary.levelNames[c];
            Level_Req_Text.text = UIDictionary.levelReqLvls[c];
            Level_Summary_Text.text = UIDictionary.levelSummarys[c];
        }
        else
        {
            Debug.Log("currentSelectedGameObject is null");
        }
    }

    //TODO MAKE THESE THOSE FUNCTIONS INTO ONE REUSABLE FUNCTION
    public void FillPanelDiffInfo()
    {
        var go = EventSystem.current.currentSelectedGameObject;
        currentSelection = (string)go.name;
        string c = currentSelection;
        if (go != null)
        {
            diff_Name_Text.text = UIDictionary.diffNames[c];
            diff_desc_Text.text = UIDictionary.diffDesc[c];
            selected_DiffName_Text.text = UIDictionary.diffNames[c];
            switch (c)
            {
                case "Easy":
                    selected_DiffName_Text.color = Color.green;
                    break;

                case "Medium":
                    selected_DiffName_Text.color = Color.yellow;
                    break;

                case "Hard":
                    selected_DiffName_Text.color = Color.red;
                    break;

                case "VeryHard":
                    selected_DiffName_Text.color = Color.black;
                    break;
            }
        }
        else
        {
            Debug.Log("currentSelectedGameObject is null");
        }
    }
}