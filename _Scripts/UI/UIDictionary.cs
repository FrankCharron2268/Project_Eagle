﻿using System.Collections.Generic;

public static class UIDictionary
{
    public static Dictionary<string, string> levelNames = new Dictionary<string, string>();
    public static Dictionary<string, string> levelReqLvls = new Dictionary<string, string>();
    public static Dictionary<string, string> levelSummarys = new Dictionary<string, string>();

    public static Dictionary<string, string> diffNames = new Dictionary<string, string>();
    public static Dictionary<string, string> diffDesc = new Dictionary<string, string>();

    public static bool IsInit = false;

    public static void Init()
    {
        //Do not Init Twice, EVER!
        if (IsInit)
        {
            return;
        }

        #region Level Names

        levelNames.Add("Level Null", "Not Available");
        levelNames.Add("Level01", "Big Nonsense");
        levelNames.Add("Level02", "Carpet Crawlers");
        levelNames.Add("Level03", "Soldier of Fortune");
        levelNames.Add("Level04", "Unrelevant Title");
        levelNames.Add("Level05", "Gotta get In");
        levelNames.Add("Level06", "To get Out");
        levelNames.Add("Level07", "Aboubadipoubadibada");
        levelNames.Add("Level08", "Trolololol");

        #endregion Level Names

        #region Level Required Lvls

        levelReqLvls.Add("Level Null", "Not Available");
        levelReqLvls.Add("Level01", "Level 1-3");
        levelReqLvls.Add("Level02", "Level 3-5");
        levelReqLvls.Add("Level03", "Level 5-7");
        levelReqLvls.Add("Level04", "Level 7-9");
        levelReqLvls.Add("Level05", "Level 9-11");
        levelReqLvls.Add("Level06", "Level 12-13");
        levelReqLvls.Add("Level07", "Level 14-15");
        levelReqLvls.Add("Level08", "Level 16-17");

        #endregion Level Required Lvls

        #region Level Summaries

        levelSummarys.Add("Level Null", "Not Available");
        levelSummarys.Add("Level01", "The description of this level describes what normally is a summary of a descriptive description that summarises tersely what boils down in a condensed text.Let us recap this laconic text.To make it short and sweet and as succinct and brief as possibly,we will condense this in a nut shell, this is a arbitraty cursory and breviloquent text :)");
        levelSummarys.Add("Level02", "There is lambs wool under my naked feet The wool is soft and warm Gives off some kind of heat A salamander scurries into fame to be destroyed Imaginary creatures are trapped in birth on celluloid The fleas cling to the golden fleece Hoping theyll find peace");
        levelSummarys.Add("Level03", "Each thought and gesture are caught in celluloid There's no hiding in my memory There's no room to avoid The crawlers cover the floor in the red ochre corridorFor my second sight of people, they've more lifeblood than before Theyre moving in time to a heavy wooden door");
        levelSummarys.Add("Level04", "Where the needle's eye is winking, closing on the poorThe carpet crawlers heed their callers Weve got to get in to get out Weve got to get in to get out Weve got to get in to get out Theres only one direction in the faces that I see Its upward to the ceiling, where the chambers said to be");
        levelSummarys.Add("Level05", "Like the forest fight for sunlight, that takes root in every tree They are pulled up by the magnet, believing they are free The carpet crawlers heed their callers Weve got to get in to get out Weve got to get in to get out Weve got to get in to get out Mild - mannered supermen are held in kryptonite And the wise and foolish virgins giggle with their bodies glowing bright");
        levelSummarys.Add("Level06", "Through the door a harvest feast is lit by candlelight Its the bottom of a staircase that spirals out of sight The carpet crawlers heed their callers We've got to get in to get out We've got to get in to get out The carpet crawlers heed their callers We've got to get in to get out We've got to get in to get out ...To get out ");
        levelSummarys.Add("Level07", "Through the door a harvest feast is lit by candlelight Its the bottom of a staircase that spirals out of sight The carpet crawlers heed their callers We've got to get in to get out We've got to get in to get out The carpet crawlers heed their callers We've got to get in to get out We've got to get in to get out ...To get out ");
        levelSummarys.Add("Level08", "Through the door a harvest feast is lit by candlelight Its the bottom of a staircase that spirals out of sight The carpet crawlers heed their callers We've got to get in to get out We've got to get in to get out The carpet crawlers heed their callers We've got to get in to get out We've got to get in to get out ...To get out ");

        #endregion Level Summaries

        #region Difficulty Names

        diffNames.Add("Easy", "Easy Something");
        diffNames.Add("Medium", "Not A Drill");
        diffNames.Add("Hard", "Mad Scientist");
        diffNames.Add("VeryHard", "Apocalypse");

        #endregion Difficulty Names

        #region Difficulty Descriptions

        diffDesc.Add("Easy", "On this difficulty, this is like this and that is like that. You take x% damage from ennemy attacks and AI is that hard. This difficulty is for those of have X xperience with games");
        diffDesc.Add("Medium", "On this difficulty, this is like this and that is like that. You take x% damage from ennemy attacks and AI is that hard. This difficulty is for those of have X xperience with games");
        diffDesc.Add("Hard", "On this difficulty, this is like this and that is like that. You take x% damage from ennemy attacks and AI is that hard. This difficulty is for those of have X xperience with games");
        diffDesc.Add("VeryHard", "On this difficulty, this is like this and that is like that. You take x% damage from ennemy attacks and AI is that hard. This difficulty is for those of have X xperience with games");

        #endregion Difficulty Descriptions

        IsInit = true;
    }
}