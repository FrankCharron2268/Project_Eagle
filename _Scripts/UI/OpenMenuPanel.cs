﻿using System.Collections;
using UnityEngine;

public class OpenMenuPanel : MonoBehaviour
{
    public GameObject menuPanel;
    private bool isActive;

    private bool m_IsKeyPressed;

    // Use this for initialization
    private void Start()
    {
        m_IsKeyPressed = false;
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        StartCoroutine(OpenMenuOnKey());
    }

    private IEnumerator OpenMenuOnKey()
    {
        if (Input.GetKey(KeyCode.Escape) && !m_IsKeyPressed)
        {
            m_IsKeyPressed = true;

            if (!isActive)
            {
                menuPanel.SetActive(true);
                isActive = true;
            }
            else if (isActive)
            {
                menuPanel.SetActive(false);
                isActive = false;
            }

            yield return new WaitForSeconds(0.2f);
            m_IsKeyPressed = false;
        }
    }

    public void CloseMenu(GameObject menuPanel)
    {
        menuPanel.SetActive(false);
        isActive = false;
    }
}