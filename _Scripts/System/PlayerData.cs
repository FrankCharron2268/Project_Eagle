﻿using UnityEngine;

public class PlayerData
{
    // Use this for initialization
    public PlayerData()
    {
        //If First Time player Opens the Game
        if (!PlayerPrefs.HasKey("FirstTime"))
        {
            PlayerPrefs.SetInt("FirstTime", 0);

            //Then we need to create Four Empty GameSlots
            CreatePlayer();
        }
    }

    private void CreatePlayer()
    {
        //Players Prefs == Save on Local PC
        PlayerPrefs.SetString("Name", "");
        PlayerPrefs.SetString("Email", "");
        PlayerPrefs.SetString("Password_Hash", "");
        PlayerPrefs.SetInt("Credits", 0);

        PlayerPrefs.SetFloat("MasterVolume", 0.5f);
        PlayerPrefs.SetFloat("MusicVolume", 0.75f);
        PlayerPrefs.SetFloat("SoundVolume", 1f);

        PlayerPrefs.SetInt("Remember_Info", 0);

        PlayerPrefs.Save();
    }

    public void ResetData()
    {
        PlayerPrefs.SetString("Name", "");
        PlayerPrefs.SetString("Email", "");
        PlayerPrefs.SetString("Password_Hash", "");
        PlayerPrefs.SetInt("Credits", 0);

        PlayerPrefs.SetFloat("MasterVolume", 0.5f);
        PlayerPrefs.SetFloat("MusicVolume", 0.75f);
        PlayerPrefs.SetFloat("SoundVolume", 1f);

        PlayerPrefs.SetInt("Remember_Info", 0);

        PlayerPrefs.Save();
    }

    public string PLAYERNAME
    {
        get { return PlayerPrefs.GetString("Name"); }
        set
        {
            PlayerPrefs.SetString("Name", value);
            PlayerPrefs.Save();
        }
    }

    public string PLAYEREMAIL
    {
        get { return PlayerPrefs.GetString("Email"); }
        set
        {
            PlayerPrefs.SetString("Email", value);
            PlayerPrefs.Save();
        }
    }

    public string PLAYERPASSWORDHASH
    {
        get { return PlayerPrefs.GetString("Password_Hash"); }
        set
        {
            PlayerPrefs.SetString("Password_Hash", value);
            PlayerPrefs.Save();
        }
    }

    public int CREDITS
    {
        get { return PlayerPrefs.GetInt("Credits"); }
        set
        {
            PlayerPrefs.SetInt("Credits", value);
            PlayerPrefs.Save();
        }
    }

    public int REMEMBERINFO
    {
        get { return PlayerPrefs.GetInt("Remember_Info"); }
        set
        {
            PlayerPrefs.SetInt("Remember_Info", value);
            PlayerPrefs.Save();
        }
    }

    public float MASTERVOLUME
    {
        get { return PlayerPrefs.GetFloat("MasterVolume"); }
        set
        {
            PlayerPrefs.SetFloat("MasterVolume", value);
            PlayerPrefs.Save();
        }
    }

    public float MUSICVOLUME
    {
        get { return PlayerPrefs.GetFloat("MusicVolume"); }
        set
        {
            PlayerPrefs.SetFloat("MusicVolume", value);
            PlayerPrefs.Save();
        }
    }

    public float SOUNDVOLUME
    {
        get { return PlayerPrefs.GetFloat("SoundVolume"); }
        set
        {
            PlayerPrefs.SetFloat("SoundVolume", value);
            PlayerPrefs.Save();
        }
    }
}