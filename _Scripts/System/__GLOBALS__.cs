﻿using UnityEngine;

public class __GLOBALS__ : MonoBehaviour
{
    private PlayerData PlayerInfo;

    public System.Random rand;

    public bool Reset_For_Debug;

    // Do not Destroy!!!
    //Also this Object has a tag __GLOBAL__
    private void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }

    // Use this for initialization
    private void Start()
    {
        rand = new System.Random();

        resetAllData();
    }

    public void resetAllData()
    {
        PlayerInfo = new PlayerData();

        if (Reset_For_Debug)
        {
            PlayerInfo.ResetData();
        }
    }

    public PlayerData PLAYERINFO
    {
        get { return this.PlayerInfo; }
        set { this.PlayerInfo = value; }
    }
}