﻿using UnityEngine;

public class GlobalCreator : MonoBehaviour
{
    public GameObject GlobalsPrefab;

    // Use this for initialization
    private void Awake()
    {
        GameObject find = GameObject.FindGameObjectWithTag("__GLOBALS__");
        if (find == null)
        {
            GameObject glob = Instantiate(GlobalsPrefab);
            glob.transform.parent = null;
        }
        else
        {
            find.GetComponent<__GLOBALS__>().resetAllData();
        }
    }
}