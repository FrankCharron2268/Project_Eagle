﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class ListWrapper
{
    public List<Transform> myList;
}

public class GameManager_LvlOne : MonoBehaviour
{
    #region Public Variables.

    public int firstEnemyCount = 1;                      //Number of this enemy tanks on first wave. Will multiply this higher on subsequent waves
    public int secondEnemyCount = 1;                 //Number of this enemy tanks on first wave. Will multiply this higher on subsequent waves
    public int turretCount = 1;                     //Number of enemy turrets on first wave. Will multiply this higher on subsequent waves
    public Font font;                           //This is the font for display UI Text.
    public GameObject menu;
    public GameObject playerHud;
    public GameObject m_PlayerPrefab;           //This is the player prefab.
    public TankManager m_PlayerTank;            //This is the players managers.

    public GameObject m_EnemyOne;           //Those are the various enemy prefabs.
    public GameObject m_EnemyTwo;
    public TankManager m_EnemyManager;
    public GameObject m_Turret;     //This stores the fixed objects such as turrets.
    public Transform[] turretSpawns;
    public Transform[] m_EnemySpawnPoints;
    public Transform[] m_PlayerSpawnPoints;
    public List<Transform> WaypointList;

    #endregion Public Variables.

    #region Private Variables.

    private TextManager txtManager;
    private GameObject playerFound;
    private GameObject[] allEnemies;
    private float spawnCooldown = 0f;
    private int round = 1;
    [HideInInspector] public float roundTimer;
    [HideInInspector] public int playerLivesLeft = 0;
    [HideInInspector] public float killCount = 0;

    private Color myColor = new Color();        //This is the color for display UI text.

    #endregion Private Variables.

    private void Awake()
    {
        try
        {
            txtManager = (TextManager)GameObject.FindGameObjectWithTag("_HUD_MANAGER_").GetComponent<TextManager>();
        }
        catch (NullReferenceException ex)
        {
            for (int i = 0; i < 42; i++)
            {
                Debug.Log("ERROR CODE : Could not find Game Manager : Restarting Scene.");
            }

            SceneManager.LoadScene("Level01");
            return;
        }
    }

    private void Start()
    {
        spawnPlayer();
        playerLivesLeft = m_PlayerPrefab.GetComponent<HealthAll>()._Lives;

        StartCoroutine("GameLoop");
        try
        {
            txtManager = (TextManager)GameObject.FindGameObjectWithTag("_HUD_MANAGER_").GetComponent<TextManager>();
        }
        catch (NullReferenceException ex)
        {
            for (int i = 0; i < 42; i++)
            {
                Debug.Log("ERROR CODE : Could not find Game Manager : Restarting Scene.");
            }

            SceneManager.LoadScene("Level01");
            return;
        }
    }

    private void Update()
    {
        roundTimer += Time.deltaTime;
        spawnCooldown += Time.deltaTime;
    }

    private void OnGUI()
    {
        float minutes = Mathf.Floor(roundTimer / 60);
        float seconds = roundTimer % 60;
        GUIStyle myStyle = new GUIStyle(GUI.skin.GetStyle("Label"));
        myStyle.alignment = TextAnchor.UpperCenter;
        Font myFont = (Font)Resources.Load("Fonts/Warning", typeof(Font));
        GUI.skin.font = myFont;
        GUI.skin.label.fontSize = 20;
        ColorUtility.TryParseHtmlString("#E9AE6AFF", out myColor);
        GUI.color = myColor;

        GUILayout.BeginArea(new Rect(Screen.width / 2 - 30, 0, 60, 28), GUI.skin.box);

        GUILayout.Label(minutes + ":" + Mathf.RoundToInt(seconds), myStyle);

        GUILayout.EndArea();
    }

    private void spawnPlayer()
    {
        int index = UnityEngine.Random.Range(0, m_PlayerSpawnPoints.Length);
        m_PlayerTank.m_Instance = Instantiate(m_PlayerPrefab, m_PlayerSpawnPoints[index].position, m_PlayerSpawnPoints[index].rotation);
    }

    //private void SpawnNPCs()
    //{
    //    for (int i = 0; i < m; i++)
    //        if (i < 1)
    //        {
    //            for (int j = 0; j < initialNpcCount; j++)
    //            {
    //                m_NPCTanks.m_Instance = Instantiate(m_NPCPrefabs, m_NPCTanks.m_SpawnPoint.position, m_NPCTanks[i].m_SpawnPoint.rotation);
    //                m_NPCTanks.SetupAI(WaypointList);
    //            }
    //        }
    //        else
    //        {
    //            for (int j = 0; j < initialNpcCount / 2; j++)
    //            {
    //                m_NPCTanks.m_Instance = Instantiate(m_NPCPrefabs, m_NPCTanks.m_SpawnPoint.position, m_NPCTanks[i].m_SpawnPoint.rotation);
    //                m_NPCTanks.SetupAI(WaypointList);
    //            }
    //        }
    //}

    //public void spawnFixedNPCs()
    //{
    //    for (int i = 0; i < m_FixedNpcs.Length; i++)
    //    {
    //        Instantiate(m_FixedNpcsPrefabs, m_FixedNpcs.m_SpawnPoint.position, m_FixedNpcs[i].m_SpawnPoint.rotation);
    //    }
    //}

    public void ResetPlayer()//reset player after a win
    {
        int index = UnityEngine.Random.Range(0, m_PlayerSpawnPoints.Length);
        m_PlayerTank.m_Instance.SetActive(false);
        m_PlayerTank.m_Instance.transform.position = m_PlayerSpawnPoints[index].position;
        m_PlayerTank.m_Instance.transform.rotation = m_PlayerSpawnPoints[index].rotation;
        m_PlayerTank.m_Instance.SetActive(true);
    }

    public IEnumerator RespawnPlayer(GameObject player)
    {
        int index = UnityEngine.Random.Range(0, m_PlayerSpawnPoints.Length);
        HealthAll playerHealth = player.GetComponent<HealthAll>();

        player.SetActive(false);

        yield return new WaitForSeconds(5f);
        player.transform.position = m_PlayerSpawnPoints[index].position;
        player.transform.rotation = m_PlayerSpawnPoints[index].rotation;

        player.SetActive(true);
        playerHealth.isDead = false;

        playerLivesLeft--;
    }

    private IEnumerator GameLoop()
    {
        yield return StartCoroutine("RoundStarting");
        yield return StartCoroutine("RoundPlaying");
        yield return StartCoroutine("RoundEnding");
    }

    private IEnumerator RoundStarting()
    {
        playerHud.SetActive(true);
        ResetPlayer();
        m_PlayerTank.DisableControl();
        yield return txtManager.StartCoroutine("RoundStartCountdown", round);

        switch (round)
        {
            case 1:
                FirstWave();
                break;

            case 2:
                SecondWave();
                break;

            case 3:
                ThirdWave();
                break;
        }

        allEnemies = GameObject.FindGameObjectsWithTag("RedTeam");
        playerFound = GameObject.FindGameObjectWithTag("GreenTeam");
        m_PlayerTank.EnableControl();
    }

    private IEnumerator RoundPlaying()
    {
        while (EnemiesLeft() && playerAlive())
        {
            yield return null;
        }
        Debug.Log("match ended");
    }

    public IEnumerator RoundEnding()
    {
        if (playerAlive())
        {
            if (round < 3)
            {
                yield return txtManager.StartCoroutine("WaveClearedMsg");
                round++;
            }
            else if (round >= 3)//check if we just passed wave 3, if so the game ends
            {
                yield return txtManager.StartCoroutine("LevelCompleteMsg");
                SceneManager.LoadScene("02a Main Menu");
            }
            StartCoroutine("GameLoop");
        }
        else
        {
            if (AsLivesLeft())
            {
                yield return txtManager.StartCoroutine("TryAgainMsg");
                RestartRound();
            }
            else
            {
                yield return txtManager.StartCoroutine("RetryOrExitMsg");
                menu.SetActive(true);
                yield return new WaitForSeconds(15);//waits 15 seconds and then exit game after player is dead and doesnt have lives left
                SceneManager.LoadScene("02a Main Menu");
            }
        }
    }

    public bool playerAlive()
    {
        if (playerFound != null)
        {
            if (playerFound.activeSelf)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public bool EnemiesLeft()
    {
        int count = 0;
        for (int i = 0; i < allEnemies.Length; i++)
        {
            if (allEnemies[i] != null)
            {
                count++;
            }
        }
        if (count > 0)
        {
            return true;
        }
        return false;
    }

    public bool AsLivesLeft()
    {
        if (playerLivesLeft > 0)
        {
            return true;
        }
        else
            return false;
    }

    public void RestartRound()
    {
        for (int i = 0; i < allEnemies.Length; i++)
        {
            if (allEnemies[i] != null)
            {
                Destroy(allEnemies[i]);
            }
        }

        StartCoroutine("GameLoop");
    }

    public void FirstWave()
    {//TODO : MAKE SPAWN POINT RANDOM BY ADDING A LIST INSTEAD OF MORE MANAGERS OR PUT A BOOLEAN TO DECIDE IF IT HAS MANY SPAWN OR NOT//partly done review to be sure
        for (int i = 0; i < firstEnemyCount * round; i++)
        {
            int index = UnityEngine.Random.Range(0, m_EnemySpawnPoints.Length);

            m_EnemyManager.m_Instance = Instantiate(m_EnemyOne, m_EnemySpawnPoints[index].position, m_EnemySpawnPoints[index].rotation);
            m_EnemyManager.SetupAI(WaypointList);
        }
        for (int i = 0; i < turretCount * round; i++)
        {
            Instantiate(m_Turret, turretSpawns[i].position, turretSpawns[i].rotation);
        }
    }

    public void SecondWave()
    {//TODO : MAKE SPAWN POINT RANDOM BY ADDING A LIST INSTEAD OF MORE MANAGERS OR PUT A BOOLEAN TO DECIDE IF IT HAS MANY SPAWN OR NOT //partly done review to be sure
        FirstWave();
        for (int i = 0; i < secondEnemyCount * (round - 1); i++)
        {
            int index = UnityEngine.Random.Range(0, m_EnemySpawnPoints.Length);
            m_EnemyManager.m_Instance = Instantiate(m_EnemyTwo, m_EnemySpawnPoints[index].position, m_EnemySpawnPoints[index].rotation);
            m_EnemyManager.SetupAI(WaypointList);
        }
    }

    public void ThirdWave()
    {
        SecondWave();
    }
}