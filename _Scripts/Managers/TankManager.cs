﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TankManager
{
    // This class is to manage various settings on a tank.
    // It works with the GameManager class to control how the tanks behave
    // and whether or not players have control of their tank in the
    // different phases of the game.
    //TODO THIS SCRIPT IS DECRECATED MIGRATE USED CODE TOWARD GAME MANAGER
    public Color m_PlayerColor;                             // This is the color this tank will be tinted.

    public string m_PlayerTeam;  //String that will be added to Gameobject Tag.

    [HideInInspector] public string m_ColoredPlayerText;    // A string that represents the player with their number colored to match their tank.
    [HideInInspector] public GameObject m_Instance;         // A reference to the instance of the tank when it is created.
    [HideInInspector] public int m_Wins;                    // The number of wins this player has so far.
    [HideInInspector] public List<Transform> m_WayPointList;
    [HideInInspector] public Camera cam;
    private TankMovement m_Movement;                        // Reference to tank's movement script, used to disable and enable control.
    private FireProjectile m_Shooting;                        // Reference to tank's shooting script, used to disable and enable control.
    private GameObject m_CanvasGameObject;                  // Used to disable the world space UI during the Starting and Ending phases of each round.
    private StateController m_StateController;              // Reference to the StateController for AI tanks

    public void SetupAI(List<Transform> wayPointList)
    {
        m_StateController = m_Instance.GetComponent<StateController>();
        m_StateController.SetupAI(true, wayPointList);

        m_Shooting = m_Instance.GetComponent<FireProjectile>();

        m_Instance.transform.tag = m_PlayerTeam;

        m_CanvasGameObject = m_Instance.GetComponentInChildren<Canvas>().gameObject;

        // Get all of the renderers of the tank.
        MeshRenderer[] renderers = m_Instance.GetComponentsInChildren<MeshRenderer>();

        // Go through all the renderers...
        for (int i = 0; i < renderers.Length; i++)
        {
            // ... set their material color to the color specific to this tank.
            renderers[i].material.color = m_PlayerColor;
        }
    }

    // Used during the phases of the game where the player shouldn't be able to control their tank.
    public void DisableControl()
    {
        m_Instance.GetComponent<TankMovement>().enabled = false;
        m_Instance.GetComponent<FireProjectile>().enabled = false;
        m_Instance.GetComponentInChildren<LookAtMouse>().enabled = false;
    }

    // Used during the phases of the game where the player should be able to control their tank.
    public void EnableControl()
    {
        m_Instance.GetComponent<TankMovement>().enabled = true;
        m_Instance.GetComponent<FireProjectile>().enabled = true;
        m_Instance.GetComponentInChildren<LookAtMouse>().enabled = true;
    }
}