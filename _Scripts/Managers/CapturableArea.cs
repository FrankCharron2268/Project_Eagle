﻿using System.Collections.Generic;
using UnityEngine;

public class CapturableArea : MonoBehaviour
{
    //public float captureRadius = 30f;
    public float capModifier = 20f;

    [HideInInspector] public float spawnTimer = 0f;
    private bool redTeam = false;
    private bool greenTeam = false;
    private CaptureController captureController;

    [HideInInspector] public bool isRedOwned = false;
    [HideInInspector] public bool isGreenOwned = false;
    [HideInInspector] public bool isNeutral = true;

    [HideInInspector] public float redCapturePct;

    [HideInInspector] public float greenCapturePct;
    public GameObject flag;
    public TextMesh textComponent;
    //public List<Transform> waypointList;
    public List<ListWrapper> WaypointList = new List<ListWrapper>();

    private void Awake()
    {
        captureController = (CaptureController)GameObject.FindGameObjectWithTag("_CaptureManager_").GetComponent<CaptureController>();
        captureController.SetFlagColor(ref flag, "Neutral");
    }

    private void Start()
    {
    }

    public void FixedUpdate()
    {
        spawnTimer += Time.fixedDeltaTime;
        setCaptureTxtInfo();
        CalculateFlagPossession();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "GreenTeam")
        {
            greenTeam = true;
        }
        if (other.tag == "RedTeam")
        {
            redTeam = true;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.tag == "GreenTeam")
        {
            greenTeam = false;
        }
        if (other.tag == "RedTeam")
        {
            redTeam = false;
        }
    }

    public void CalculateFlagPossession()
    {
        if (redCapturePct >= 100)
        {
            if (isNeutral)
            {
                captureController.redTeamFlagCnt += 1;
                resetSpawnTimer();
                isRedOwned = true;
                isNeutral = false;
                captureController.SetFlagColor(ref flag, "Red");
            }
            else if (isGreenOwned)
            {
                captureController.greenTeamFlagCnt -= 1;
                captureController.redTeamFlagCnt += 1;
                resetSpawnTimer();
                isGreenOwned = false;
                isRedOwned = true;
                isNeutral = false;
                captureController.SetFlagColor(ref flag, "Red");
            }
            else if (isRedOwned)
            {
                redCapturePct = 100f;
                greenCapturePct = 0f;
            }
        }
        if (greenCapturePct >= 100)
        {
            if (isNeutral)
            {
                resetSpawnTimer();
                captureController.greenTeamFlagCnt += 1;
                isGreenOwned = true;
                isNeutral = false;
                captureController.SetFlagColor(ref flag, "Green");
            }
            else if (isRedOwned)
            {
                resetSpawnTimer();
                captureController.greenTeamFlagCnt += 1;
                captureController.redTeamFlagCnt -= 1;
                isGreenOwned = true;
                isRedOwned = false;
                captureController.SetFlagColor(ref flag, "Green");
            }
            else if (isGreenOwned)
            {
                redCapturePct = 0f;
                greenCapturePct = 100f;
            }
        }

        if (greenCapturePct <= 50 && redCapturePct <= 51 || greenCapturePct <= 51 && redCapturePct <= 50)
        {
            if (isRedOwned)
            {
                captureController.redTeamFlagCnt -= 1;
                isRedOwned = false;
                isNeutral = true;
            }
            else if (isGreenOwned)
            {
                captureController.greenTeamFlagCnt -= 1;
                isGreenOwned = false;
                isNeutral = true;
            }

            captureController.SetFlagColor(ref flag, "Neutral");
        }
        if (greenTeam == true)
        {
            greenCapturePct += Time.deltaTime * capModifier;
            redCapturePct -= Time.deltaTime * capModifier;
        }

        if (redTeam == true)
        {
            greenCapturePct -= Time.deltaTime * capModifier;
            redCapturePct += Time.deltaTime * capModifier;
        }
        if (redTeam == true && greenTeam == true)
        {
            redCapturePct = redCapturePct;
            greenCapturePct = greenCapturePct;
        }
        if (redCapturePct <= 0)
        {
            redCapturePct = 0;
        }
        if (greenCapturePct <= 0)
        {
            greenCapturePct = 0;
        }
    }

    public void setCaptureTxtInfo()
    {
        textComponent.text = "Red:" + redCapturePct.ToString("0") + " Green:" + greenCapturePct.ToString("0");
    }

    public void resetSpawnTimer()
    {
        spawnTimer = 0;
    }
}