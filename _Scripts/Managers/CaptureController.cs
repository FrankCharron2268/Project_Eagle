﻿using System.Collections.Generic;
using UnityEngine;

public class CaptureController : MonoBehaviour
{
    public Texture flagGreen;
    public Texture flagRed;
    public Texture flagNeutral;
    public float spawnRate = 5.0f;

    [HideInInspector] public int greenTeamFlagCnt = 0;
    [HideInInspector] public int redTeamFlagCnt = 0;
    [HideInInspector] public bool greenTeam = false;
    [HideInInspector] public bool redTeam = false;

    private GameObject[] captureGO;
    private CapturableArea[] capturableAreas;
    private Vector3[] spawnPositions;
    public GameObject npcAllyPrefab;
    public TankManager m_NPC;
    public List<ListWrapper> waypointList = new List<ListWrapper>();

    private void Start()
    {
        captureGO = GameObject.FindGameObjectsWithTag("CaptureArea");
        capturableAreas = new CapturableArea[captureGO.Length];
        spawnPositions = new Vector3[captureGO.Length];
        for (int i = 0; i < captureGO.Length; i++)
        {
            capturableAreas[i] = captureGO[i].GetComponent<CapturableArea>();
            spawnPositions[i] = captureGO[i].transform.position;
        }
    }

    private void Update()
    {
        spawnAlliedNPCs();
    }

    private void OnGUI()
    {
        GUI.Box(new Rect(Screen.width / 2, 10, 300, 25), "Bases Captured : Red : " + redTeamFlagCnt.ToString("0") + " | | " + " Green: " + greenTeamFlagCnt.ToString("0"));
    }

    public void SetFlagColor(ref GameObject flag, string color)
    {
        switch (color)
        {
            case "Neutral":
                flag.GetComponent<SkinnedMeshRenderer>().material.mainTexture = flagNeutral;
                break;

            case "Red":
                flag.GetComponent<SkinnedMeshRenderer>().material.mainTexture = flagRed;
                break;

            case "Green":
                flag.GetComponent<SkinnedMeshRenderer>().material.mainTexture = flagGreen;
                break;
        }
    }

    private void spawnAlliedNPCs()
    {
        for (int i = 0; i < capturableAreas.Length; i++)
        {
            CapturableArea area;
            area = capturableAreas[i];
            bool isTime = CheckSpawnTimer(i);

            if (!area.isNeutral && isTime)//si t'elle base est pas neutre.
            {
                if (area.isRedOwned)//si elle est au rouge spawn des rouges.
                {
                    setupAlliedNpcs(i, "RedTeam");
                }
                else if (area.isGreenOwned)//si elle est au vert spawn des verts.
                {
                    setupAlliedNpcs(i, "GreenTeam");
                    //spawn npc tagged green team
                }
            }
        }
    }

    private void setupAlliedNpcs(int i, string team)
    {
        int nb = 0;
        if (team == "GreenTeam")
        {
            for (int a = 0; a < 5; a++)
            {
                m_NPC.m_Instance = Instantiate(npcAllyPrefab, spawnPositions[i], Quaternion.identity) as GameObject;

                m_NPC.m_PlayerTeam = team;
                m_NPC.SetupAI(capturableAreas[i].WaypointList[0].myList);
                nb++;
            }
        }
        if (team == "RedTeam")
        {
            for (int a = 0; a < 5; a++)
            {
                m_NPC.m_Instance = Instantiate(npcAllyPrefab, spawnPositions[i], Quaternion.identity) as GameObject;

                m_NPC.m_PlayerTeam = team;
                m_NPC.SetupAI(capturableAreas[i].WaypointList[1].myList);
                nb++;
            }
        }
        // Setup the AI tanks
    }

    public bool CheckSpawnTimer(int i)
    {
        CapturableArea area = capturableAreas[i];
        float temp = area.spawnTimer; //get the spawntimer for this component
        if (temp >= spawnRate)
        {
            area.spawnTimer = 0f; // reset spawn timer
            return true;
        }
        else
            return false;
    }
}