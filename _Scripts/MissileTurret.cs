﻿using UnityEngine;

public class MissileTurret : MonoBehaviour
{
    public GameObject detectionCollider;
    public GameObject missile;
    public Transform eyes;

    public Transform[] fireTransforms;
    public float fireCooldown = 3f;
    private float coolTimer = 0;
    private bool _switch = false;
    [HideInInspector] public GameObject targetToShoot;
    //public AudioClip missileFiring;

    private AudioSource audioSource;
    private string myTag;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (targetToShoot)
        {
            transform.LookAt(targetToShoot.transform);
        }

        coolTimer += Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        myTag = transform.tag;
        if (myTag == "GreenTeam" && other.CompareTag("RedTeam"))
        {
            targetToShoot = other.gameObject;
            transform.LookAt(targetToShoot.transform);
            if (CheckIfCoolDownElapsed())
            {
                LaunchMissile();
                coolTimer = 0f;
            }
        }
        if (myTag == "RedTeam" && other.CompareTag("GreenTeam"))
        {
            targetToShoot = other.gameObject;
            transform.LookAt(targetToShoot.transform);
            if (CheckIfCoolDownElapsed())
            {
                LaunchMissile();
                coolTimer = 0f;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        myTag = transform.tag;
        if (myTag == "GreenTeam" && other.CompareTag("RedTeam"))
        {
            targetToShoot = other.gameObject;
            transform.LookAt(targetToShoot.transform);
            if (CheckIfCoolDownElapsed())
            {
                LaunchMissile();
                coolTimer = 0f;
            }
        }
        if (myTag == "RedTeam" && other.CompareTag("GreenTeam"))
        {
            targetToShoot = other.gameObject;
            if (CheckIfCoolDownElapsed())
            {
                transform.LookAt(targetToShoot.transform);
                LaunchMissile();
                coolTimer = 0f;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        targetToShoot = null;
    }

    public int altFire()
    {
        if (_switch == false)
        {
            _switch = true;
            return 1;
        }
        else
        {
            _switch = false;
            return 0;
        }
    }

    public bool CheckIfCoolDownElapsed()
    {
        return (coolTimer >= fireCooldown);
    }

    public void LaunchMissile()
    {
        var temp = Instantiate(missile, fireTransforms[altFire()].position, gameObject.transform.rotation);

        temp.GetComponent<HomingProjectile>().targetObjDestination = targetToShoot.transform.gameObject;
        temp.GetComponent<HomingProjectile>().myTag = myTag;
        audioSource.Play();
    }
}