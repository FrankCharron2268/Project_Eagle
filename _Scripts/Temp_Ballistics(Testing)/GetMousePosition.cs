﻿using UnityEngine;

public class GetMousePosition : MonoBehaviour
{
    public LayerMask clickMask;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 clickPosition = -Vector3.one;
            //METHOD 1 : ScreenToWorldPoint (very limited)
            //clickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition + new Vector3(0, 0, 5f));

            //Method 2 : Raycast using Colliders with max range (NOTE this one works best)
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 1000f, clickMask))//this 100f is the range distance
            {
                clickPosition = hit.point;
            }

            //Method3 : Raycast using Plane (this one seems to be useful on a flat terrain)
            //Plane plane = new Plane(Vector3.up, 0f);
            //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            //float distanceToPlane;

            //if (plane.Raycast(ray, out distanceToPlane))
            //{
            //    clickPosition = ray.GetPoint(distanceToPlane);
            //}

            Debug.Log(clickPosition);
        }
    }
}