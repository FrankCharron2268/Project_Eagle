﻿using System.Collections;
using UnityEngine;

public class OnDrawGizmos : MonoBehaviour
{
    #region Public Variables

    [Range(0f, 90f)]
    public float wantedAngle = 45f;
    [Range(0f, 100f)]
    public float initialSpeed = 5;
    [Range(3, 30)]
    public int pointCount = 10;

    public GameObject projectile;
    public Transform targetGO;
    public Transform muzzleGO;

    #endregion Public Variables

    #region Private Variables

    private float totalTime;

    #endregion Private Variables

    #region Main Methods

    #region Utility Methods

    private void DrawGizmos()
    {
        Vector3 forwardVec = transform.right;
        Vector3 projectileVec = Quaternion.AngleAxis(wantedAngle, Vector3.forward) * forwardVec;

        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + forwardVec);

        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(transform.position, transform.position + (projectileVec * initialSpeed));

        //float maxHeight = GetMaxHeight(initialSpeed);
        //Gizmos.color = Color.green;
        //Gizmos.DrawLine(transform.position, transform.position + (Vector3.up * maxHeight));

        Gizmos.color = Color.cyan;
        float adjSide = getCos(initialSpeed, wantedAngle);
        Gizmos.DrawLine(transform.position, transform.position + (transform.right * adjSide));

        Gizmos.color = Color.green;
        float oppSide = GetSin(initialSpeed, wantedAngle);
        Gizmos.DrawLine(transform.position + (transform.right * adjSide), (transform.position + (transform.right * adjSide)) + (transform.up * oppSide));

        //Range
        float timeInAir = GetTimeInAir(oppSide);
        float range = adjSide * timeInAir;
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + (transform.right * range));

        //Plot trajectory
        float timeStep = timeInAir / (float)pointCount;
        float curTime = 0f;
        for (int i = 0; i <= pointCount; i++)
        {
            float x = GetXPoint(initialSpeed, wantedAngle, curTime);
            float y = GetYPoint(initialSpeed, wantedAngle, curTime);
            Vector3 pointPos = new Vector3(x, y, 0f);
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(pointPos, 0.05f);

            curTime += timeStep;
        }

        //Display Angle of Reach
        if (targetGO && muzzleGO)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(transform.position, 0.15f);
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(muzzleGO.position, 0.15f);

            Gizmos.color = Color.red;
            Gizmos.DrawSphere(targetGO.position, 0.15f);
            Gizmos.color = Color.black;
            Gizmos.DrawLine(transform.position, targetGO.position);

            // Vector3 directionToTarget = targetGO.position - transform.position;
            //float finalAngle = GetAngleOfReach(muzzleGO.position, directionToTarget, initialSpeed, Physics.gravity.magnitude);
            // Debug.Log(finalAngle);

            //Vector3 cannonVec = transform.right * 2f;
            //cannonVec = Quaternion.AngleAxis(finalAngle, transform.forward) * cannonVec;
            //Gizmos.color = Color.magenta;
            //Gizmos.DrawLine(transform.position, transform.position + cannonVec);
        }
    }

    #endregion Utility Methods

    // Use this for initialization
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
    }

    #endregion Main Methods

    #region Math Methods

    private float GetMaxHeight(float aSpeed)
    {
        float maxHeight = (aSpeed * aSpeed) / (2f * Physics.gravity.magnitude);

        return maxHeight;
    }

    private float GetTimeInAir(float aSpeed)
    {
        float timeInAir = (-aSpeed - aSpeed) / (Physics.gravity.y);
        return timeInAir;
    }

    private float GetSin(float hypotneuse, float aAngle) //for draw triangle
    {
        float oppositeSide = hypotneuse * Mathf.Sin(aAngle * Mathf.Deg2Rad);
        return oppositeSide;
    }

    private float getCos(float hypotneuse, float aAngle) //for draw triangle
    {
        float adjacentSide = hypotneuse * Mathf.Cos(aAngle * Mathf.Deg2Rad);
        return adjacentSide;
    }

    private float GetXPoint(float aSpeed, float aAngle, float t)
    {
        float x = (aSpeed * Mathf.Cos(aAngle * Mathf.Deg2Rad)) * t;
        return x;
    }

    private float GetYPoint(float aSpeed, float aAngle, float t)
    {
        float y = ((aSpeed * Mathf.Sin(aAngle * Mathf.Deg2Rad)) - ((Physics.gravity.magnitude / 2f) * t)) * t;
        return y;
    }

    private IEnumerator StartStopWatch(GameObject projectile)
    {
        while (projectile.transform.position.y >= 0f)
        {
            totalTime += Time.deltaTime;
            yield return null;
        }
        Debug.Log(totalTime);
    }

    #endregion Math Methods
}