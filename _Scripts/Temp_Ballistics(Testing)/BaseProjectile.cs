﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BaseProjectile : MonoBehaviour
{
    #region Public Variables

    public float lifeSpan = 5f;

    #endregion Public Variables

    #region Private Variables

    private Rigidbody curRB;

    #endregion Private Variables

    #region Main Methods

    // Use this for initialization
    private void Awake()
    {
        curRB = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    private void Update()
    {
        Destroy(gameObject, lifeSpan);
    }

    #endregion Main Methods

    public void LaunchProjectile(float aSpeed)
    {
        if (curRB)
        {
            Debug.Log("LaunchProjectile Called");
            curRB.AddForce(transform.forward * aSpeed, ForceMode.Impulse);
        }
    }
}