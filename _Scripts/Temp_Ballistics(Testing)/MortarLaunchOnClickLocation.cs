﻿using UnityEngine;

public class MortarLaunchOnClickLocation : MonoBehaviour

{
    public Rigidbody ball;

    // public Transform target;
    public LayerMask clickMask;

    private Vector3 clickPosition = -Vector3.one;

    public float height = 25;
    public float gravity = -18;

    private void Start()
    {
        ball.useGravity = false;
    }

    private void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Launch();
        }
    }

    public void Launch()
    {
        Physics.gravity = Vector3.up * gravity;
        ball.useGravity = true;

        ball.velocity = CalculateLaunchVelocity(GetMouseClickPosition());
    }

    private Vector3 CalculateLaunchVelocity(Vector3 target)
    {
        float displacementY = target.y - ball.position.y;
        Vector3 displacementXZ = new Vector3(target.x - ball.position.x, 0, target.z - ball.position.z);

        Vector3 velocityY = Vector3.up * Mathf.Sqrt(-2 * gravity * height);
        Vector3 velocityXZ = displacementXZ / (Mathf.Sqrt(-2 * height / gravity) + Mathf.Sqrt(2 * (displacementY - height) / gravity));

        return velocityXZ + velocityY;
    }

    private Vector3 GetMouseClickPosition()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 1000f, clickMask))//this 1000f is the range distance
        {
            clickPosition = hit.point;
        }
        Debug.Log(clickPosition);
        return clickPosition;
    }
}