﻿using UnityEngine;

public class Cannon : MonoBehaviour
{
    public GameObject cannonballInstance;
    public Transform FiringPoint;

    [SerializeField]
    [Range(10f, 80f)]
    private float angle = 45f;
    private string myTag;

    private void Start()
    {
        myTag = gameObject.transform.tag;
    }

    private void Update()
    {
        if (Input.GetKeyUp("space"))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo))
            {
                CmdFire(hitInfo.point);
            }
        }
    }

    public void CmdFire(Vector3 point)
    {
        Vector3 velocity;

        Vector3 dir = point - FiringPoint.position; // get Target Direction
        float height = dir.y; // get height difference
        dir.y = 0; // retain only the horizontal difference
        float dist = dir.magnitude; // get horizontal direction
        float a = angle * Mathf.Deg2Rad; // Convert angle to radians
        dir.y = dist * Mathf.Tan(a); // set dir to the elevation angle.
        dist += height / Mathf.Tan(a); // Correction for small height differences

        // Calculate the velocity magnitude
        velocity = Mathf.Sqrt(dist * Physics.gravity.magnitude / Mathf.Sin(2 * a)) * dir.normalized; // Return a normalized vector.

        Debug.Log("Firing at " + point + " velocity " + velocity);

        GameObject temp = Instantiate(cannonballInstance, FiringPoint.position, new Quaternion());
        temp.GetComponent<Projectile>().myTag = myTag;

        temp.GetComponent<Rigidbody>().velocity = velocity;
    }

    //Vector3 BallisticVelocity(Vector3 destination, float angle)
    //{
    //    Vector3 dir = destination - transform.position; // get Target Direction
    //    float height = dir.y; // get height difference
    //    dir.y = 0; // retain only the horizontal difference
    //    float dist = dir.magnitude; // get horizontal direction
    //    float a = angle * Mathf.Deg2Rad; // Convert angle to radians
    //    dir.y = dist * Mathf.Tan(a); // set dir to the elevation angle.
    //    dist += height / Mathf.Tan(a); // Correction for small height differences

    //    // Calculate the velocity magnitude
    //    float velocity = Mathf.Sqrt(dist * Physics.gravity.magnitude / Mathf.Sin(2 * a));
    //    return velocity * dir.normalized; // Return a normalized vector.
    //}
}