﻿using UnityEngine;

public class Trajectory_Debug : MonoBehaviour
{
    #region Public Variables

    [Range(0f, 90f)]
    public float wantedAngle = 45f;
    [Range(0f, 100f)]
    public float initialSpeed = 20;
    [Range(3, 30)]
    public int pointCount = 10;

    public GameObject projectile;
    public Transform targetGO;
    public Transform muzzleGO;

    public bool directFire = false;

    #endregion Public Variables

    #region Private Variables

    //private float totalTime;

    #endregion Private Variables

    #region Main Methods

    private void Start()
    {
    }

    private void Update()
    {
        if (projectile)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Vector3 directionToTarget = targetGO.position - transform.position;
                float finalAngle = GetAngleOfReach(muzzleGO.position, directionToTarget, initialSpeed, Physics.gravity.magnitude);

                //Vector3 projectileVec = Quaternion.AngleAxis(finalAngle, Vector3.forward) * transform.right;

                Vector3 cannonVec = transform.right * 2f;
                cannonVec = Quaternion.AngleAxis(finalAngle, transform.forward) * cannonVec;
                GameObject curProjectile = (GameObject)Instantiate(projectile, transform.position, Quaternion.LookRotation(cannonVec));
                if (curProjectile)
                {
                    curProjectile.SendMessage("LaunchProjectile", initialSpeed);
                    //float timeInAir = GetTimeInAir(initialSpeed);
                    //Debug.Log(timeInAir);

                    //totalTime = 0f;
                    //StartCoroutine("StartStopWatch", curProjectile);
                }
            }
        }
    }

    #endregion Main Methods

    #region Utility Methods

    private void OnDrawGizmos()
    {
        //Display Angle of Reach
        if (targetGO && muzzleGO)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(transform.position, 0.15f);
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(muzzleGO.position, 0.15f);

            Gizmos.color = Color.red;
            Gizmos.DrawSphere(targetGO.position, 0.15f);
            Gizmos.color = Color.black;
            Gizmos.DrawLine(transform.position, targetGO.position);

            Vector3 directionToTarget = targetGO.position - transform.position;
            float finalAngle = GetAngleOfReach(muzzleGO.position, directionToTarget, initialSpeed, Physics.gravity.magnitude);
            Debug.Log(finalAngle);

            Vector3 cannonVec = transform.right * 2f;
            cannonVec = Quaternion.AngleAxis(finalAngle, transform.forward) * cannonVec;
            Gizmos.color = Color.magenta;
            Gizmos.DrawLine(transform.position, transform.position + cannonVec);
        }
    }

    #endregion Utility Methods

    #region Math Methods

    private float GetAngleOfReach(Vector3 muzzlePos, Vector3 aDirection, float v, float g)
    {
        float y = muzzlePos.y - transform.position.y;
        float x = aDirection.magnitude;

        //float sqrt = (v * v * v * v) - g * ((g * (x * x)) + 2f * (y * (v * v)));
        //float sqrt = (v * v * v * v) - g * ((g * (v * v)) + 2f * (y * (v * v)));

        if ((float)muzzleGO.position.x <= (float)targetGO.position.x)
        {
            float sqrt = (v * v * v * v) - g * ((g * (v * v)) + 2f * (y * (v * v)));
            sqrt = Mathf.Sqrt(sqrt);
            if (sqrt <= 0f)
            {
                Debug.Log("Out of Range");
                return 0f;
            }
            if (directFire) //shallow angle
            {
                float angle = (float)Mathf.Atan(((v * v) - sqrt) / (g * x)) * (float)Mathf.Rad2Deg;
                return angle;
                //Debug.Log(angle);
            }
            else //steep angle
            {
                float angle = (float)Mathf.Atan(((v * v) + sqrt) / (g * x)) * (float)Mathf.Rad2Deg;

                return angle;
                //Debug.Log(angle);
            }
        }
        else if ((float)muzzleGO.position.x >= (float)targetGO.position.x)
        {
            float sqrt = (v * v * v * v) - g * ((g * (v * v)) + 2f * (y * (v * v)));
            sqrt = Mathf.Sqrt(sqrt);
            if (sqrt <= 0f)
            {
                Debug.Log("Out of Range");
                return 0f;
            }

            if (directFire) //shallow angle
            {
                float angle = (float)Mathf.Atan(((v * v) - sqrt) / (g * x)) * (float)Mathf.Rad2Deg;
                return -angle - 180f;
                //Debug.Log(angle);
            }
            else //steep angle
            {
                float angle = (float)Mathf.Atan(f: ((v * v) + sqrt) / (g * x)) * (float)Mathf.Rad2Deg;

                return -angle - 180f;
                //Debug.Log(angle);
            }
        }
        return 0f;
    }

    //private IEnumerator StartStopWatch(GameObject projectile)
    //{
    //    while (projectile.transform.position.y >= 0f)
    //    {
    //        totalTime += Time.deltaTime;
    //        yield return null;
    //    }
    //    Debug.Log(totalTime);
    //}

    #endregion Math Methods
}