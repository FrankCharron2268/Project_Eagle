﻿using UnityEngine;

public class FireInStraightLine : MonoBehaviour
{
    public Transform FireTransform;
    public GameObject Shell;
    public float ShellForwardForce;

    public float cooldown;
    private float m_LastShot;

    private void Start()
    {
        m_LastShot = -cooldown;
    }

    private void FixedUpdate()
    {
        if (Input.GetMouseButton(0) && Time.time > (m_LastShot + cooldown))
        {
            m_LastShot = Time.time;

            GameObject tempBulletHandler = Instantiate(Shell, FireTransform.position, FireTransform.rotation) as GameObject;

            tempBulletHandler.transform.Rotate(Vector3.up * 90);

            Rigidbody tempRigidbody;
            tempRigidbody = tempBulletHandler.GetComponent<Rigidbody>();

            tempRigidbody.GetComponent<Rigidbody>().velocity = ShellForwardForce * FireTransform.forward;

            tempRigidbody.AddForce(transform.forward, ForceMode.Impulse);

            Destroy(tempBulletHandler, 10.0f);
        }
    }
}