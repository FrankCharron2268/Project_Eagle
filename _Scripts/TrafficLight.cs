﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficLight : MonoBehaviour
{
    public Light[] greenLight;
    public Light[] yellowLight;
    public Light[] redLight;
    public bool isOpposed;

    private void Start()
    {
        StartCoroutine("LightSync");
    }

    private IEnumerator LightSync()
    {
        if (!isOpposed)
        {
            yield return StartCoroutine("GreenLight");
            yield return StartCoroutine("YellowLight");
            yield return StartCoroutine("RedLight");
        }
        else
        {
            yield return StartCoroutine("RedLight");
            yield return StartCoroutine("YellowLight");
            yield return StartCoroutine("GreenLight");
        }
        StartCoroutine("LightSync");
    }

    private IEnumerator GreenLight()
    {
        for (int i = 0; i < greenLight.Length; i++)
        {
            greenLight[i].intensity = 250f;
        }
        yield return new WaitForSeconds(15f);
        for (int i = 0; i < greenLight.Length; i++)
        {
            greenLight[i].intensity = 6f;
        }
    }

    private IEnumerator YellowLight()
    {
        for (int i = 0; i < yellowLight.Length; i++)
        {
            yellowLight[i].intensity = 250f;
        }
        yield return new WaitForSeconds(5f);
        for (int i = 0; i < yellowLight.Length; i++)
        {
            yellowLight[i].intensity = 6f;
        }
    }

    private IEnumerator RedLight()
    {
        for (int i = 0; i < redLight.Length; i++)
        {
            redLight[i].intensity = 250f;
        }
        yield return new WaitForSeconds(20f);
        for (int i = 0; i < greenLight.Length; i++)
        {
            redLight[i].intensity = 6f;
        }
    }
}