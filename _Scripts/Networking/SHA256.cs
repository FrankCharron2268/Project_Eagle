﻿using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
    [ComVisible(true)]
    public abstract class SHA256 : HashAlgorithm
    {
        protected SHA256()
        {
            this.HashSizeValue = 256;
        }

#pragma warning disable CS0436 // Type conflicts with imported type

        public static new SHA256 Create()
#pragma warning restore CS0436 // Type conflicts with imported type
        {
            return SHA256.Create("System.Security.Cryptography.SHA256");
        }

        public static new SHA256 Create(string hashName)
        {
            return (SHA256)CryptoConfig.CreateFromName(hashName);
        }
    }
}