﻿using System;
using UnityEngine;

public class HealthAll : MonoBehaviour
{
    #region Public Variables.

    public GameObject cameraP; //for player only
    public int _Lives = 3;
    public float maxHealth = 1000f;
    public RectTransform healthBar;
    public GameObject explosionPrefabFX;
    public bool destroyOnDeath = false;

    #endregion Public Variables.

    #region Private Variables.

    private GameManager_LvlOne manager;
    private ParticleSystem particleSysFX;
    [HideInInspector] public bool isDead = false;
    [HideInInspector] public Vector3 spawnPoint;

    private float currentHealth;

    #endregion Private Variables.

    private void OnEnable()
    {
        currentHealth = maxHealth;
    }

    private void Update()
    {
        OnChangeHealth(currentHealth);
    }

    private void Start()
    {
        try
        {
            manager = (GameManager_LvlOne)GameObject.FindGameObjectWithTag("__GAME_MANAGER__").GetComponent<GameManager_LvlOne>();
        }
        catch (NullReferenceException ex)
        {
        }

        particleSysFX = explosionPrefabFX.GetComponent<ParticleSystem>();

        spawnPoint = transform.position;
        isDead = false;
    }

    public void TakeDamage(float damage)
    {
        currentHealth -= damage;
        if (currentHealth <= 0)
        {
            Ondeath();
        }
    }

    public void Ondeath()
    {
        Vector3 explosionPos = transform.position + new Vector3(0, 3, 0);
        if (!destroyOnDeath)
        {
            Instantiate(explosionPrefabFX, explosionPos, transform.rotation);
            _Lives--;
            cameraP.transform.parent = null;
            if (manager.AsLivesLeft())
            {
                manager.StartCoroutine("RespawnPlayer", gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }
        else if (destroyOnDeath)
        {
            Destroy(gameObject);
            manager.killCount++;
        }
        Instantiate(explosionPrefabFX, explosionPos, transform.rotation);
        explosionPrefabFX.transform.parent = null;
        isDead = true;

        explosionPrefabFX.transform.parent = null;
    }

    private void OnChangeHealth(float currentHealth)
    {
        float x = maxHealth / 100;
        healthBar.sizeDelta = new Vector2(currentHealth / x, healthBar.sizeDelta.y);
    }
}