﻿using UnityEngine;

public class SensorTowerRotate : MonoBehaviour
{
    public float _turnRate;
    public bool _turnClockWise;
    public bool _turnUpDown;

    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
        if (!_turnUpDown)
        {
            if (!_turnClockWise)
            {
                this.transform.Rotate(0, 0, -_turnRate * Time.deltaTime);
            }
            else
            {
                this.transform.Rotate(0, 0, _turnRate * Time.deltaTime);
            }
        }
        else
        {
            this.transform.Rotate(_turnRate * Time.deltaTime, 0, 0);
        }
    }
}