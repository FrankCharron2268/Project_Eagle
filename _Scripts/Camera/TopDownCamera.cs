﻿using UnityEngine;

public class TopDownCamera : MonoBehaviour
{
    public Transform m_Target;
    [SerializeField]
    private float m_Height = 10f;
    [SerializeField]
    private float m_Distance = 20f;
    [SerializeField]
    private float m_Angle = 45f;
    [SerializeField]
    private float m_SmoothSpeed = 0.5f;

    private void Start()
    {
        HandleCamera();
    }

    // Update is called once per frame
    private void Update()
    {
        HandleCamera();
    }

    protected virtual void HandleCamera()
    {
        if (!m_Target)
        {
            return;
        }
        //Build world position vector
        Vector3 worldPosition = (Vector3.forward * -m_Distance) + (Vector3.up * m_Height);
        //Debug.DrawLine(m_Target.position, worldPosition, Color.red);

        //build our Rotated vector.
        Vector3 rotatedVector = Quaternion.AngleAxis(m_Angle, Vector3.up) * worldPosition;
        // Debug.DrawLine(m_Target.position, rotatedVector, Color.green);

        //Move our position.
        Vector3 flatTargetPosition = m_Target.position;
        flatTargetPosition.y = 0f;
        Vector3 finalPosition = flatTargetPosition + rotatedVector;
        //Debug.DrawLine(m_Target.position, finalPosition, Color.blue);

        transform.position = finalPosition;
        transform.LookAt(m_Target.position);
    }
}