﻿using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public Transform playerTransform;
    public int depth = -16;

    // Update is called once per frame
    private void Update()
    {
        if (playerTransform != null)
        {
            transform.position = playerTransform.position + new Vector3(0, 25, depth);
        }
    }

    public void setTarget(Transform target)
    {
        playerTransform = target;
    }
}