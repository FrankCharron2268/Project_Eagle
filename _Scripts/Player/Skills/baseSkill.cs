﻿using UnityEngine;

public abstract class BaseSkill
{
    //test
    public int BaseDamage { get; set; }

    public int Range { get; set; }

    public AudioSource Sound { get; set; }

    public int EnergyCost { get; set; }

    public BaseSkill(int dmg, int range, AudioSource sound, int cost)
    {
        this.BaseDamage = dmg;
        this.Range = range;
        this.Sound = sound;
        this.EnergyCost = cost;
    }

    public abstract int LaunchSkill();
}