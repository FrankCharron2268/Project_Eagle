﻿using UnityEngine;

public class SkillOne : BaseSkill
{
    private int extra_Info;

    //test
    public SkillOne(int dmg, int range, AudioSource sound, int cost) : base(dmg, range, sound, cost)
    {
    }

    public override int LaunchSkill()
    {
        return Mortar();
    }

    private int Mortar()
    {
        return 1;
    }
}