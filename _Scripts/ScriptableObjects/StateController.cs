﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class StateController : MonoBehaviour
{
    public bool asTurret = false;
    public bool RandomizeWaypoints = false;
    public State currentState;
    public EnemyStats enemyStats;
    public Transform eyes;
    public State remainState;
    public GameObject turretToTurn;
    [HideInInspector] public float roundTimer;
    [HideInInspector] public NavMeshAgent navMeshAgent;
    [HideInInspector] public FireProjectile tankShooting;
    [HideInInspector] public Kamikaze kamikaze;
    [HideInInspector] public List<Transform> wayPointList;
    [HideInInspector] public int nextWayPoint;
    [HideInInspector] public Transform targetToChase;
    [HideInInspector] public float stateTimeElapsed;
    [HideInInspector] public GameManager_LvlOne manager;
    private bool aiActive;

    private void Awake()
    {
        kamikaze = GetComponent<Kamikaze>();
        tankShooting = GetComponent<FireProjectile>();
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    public void SetupAI(bool aiActivationFromTankManager, List<Transform> wayPointsFromTankManager)
    {
        wayPointList = wayPointsFromTankManager;
        aiActive = aiActivationFromTankManager;

        if (aiActive)
        {
            navMeshAgent.enabled = true;
        }
        else
        {
            navMeshAgent.enabled = false;
        }
    }

    private void Update()
    {
        if (targetToChase != null)
        {
            if (asTurret)
            {
                LookAtTarget();
            }
        }
        if (!aiActive)
        {
            return;
        }
        else
        {
            currentState.UpdateState(this);
        }
    }

    private void Start()
    {
        try
        {
            manager = (GameManager_LvlOne)GameObject.FindGameObjectWithTag("__GAME_MANAGER__").GetComponent<GameManager_LvlOne>();
        }
        catch (NullReferenceException ex)
        {
            for (int i = 0; i < 42; i++)
            {
                Debug.Log("ERROR CODE : Could not find Game Manager : Restarting Scene.");
            }

            SceneManager.LoadScene("Level01");
            return;
        }
    }

    private void OnDrawGizmos()
    {
        if (currentState != null && eyes != null)
        {
            Gizmos.color = currentState.sceneGizmoColor;
            Gizmos.DrawWireSphere(eyes.position, enemyStats.lookSphereCastRadius);
        }
    }

    public void TransitionToState(State nextState)
    {
        if (nextState != remainState)
        {
            currentState = nextState;
        }
    }

    public bool CheckIfCountDownElapsed(float duration)
    {
        stateTimeElapsed += Time.deltaTime;
        return (stateTimeElapsed >= duration);
    }

    private void OnExitState()
    {
        stateTimeElapsed = 0;
    }

    public void LookAtTarget()
    {
        if (targetToChase != null)
        {
            Vector3 targetPoint = targetToChase.transform.position;

            // Determine the target rotation.  This is the rotation if the transform looks at the target point.
            Quaternion targetRotation = Quaternion.LookRotation(targetPoint - turretToTurn.transform.position);

            // Smoothly rotate towards the target point.
            turretToTurn.transform.rotation = Quaternion.Slerp(turretToTurn.transform.rotation, targetRotation, 10 * Time.deltaTime);
        }
    }
}