﻿using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Chase")]
public class ChaseAction : Action
{
    public override void Act(StateController controller)
    {
        Chase(controller);
    }

    private void Chase(StateController controller)
    {
        if (controller.navMeshAgent)
        {
            if (controller.targetToChase != null)
            {
                controller.navMeshAgent.destination = controller.targetToChase.position;
                controller.navMeshAgent.isStopped = false;
                controller.navMeshAgent.stoppingDistance = 40f;
            }
        }
    }
}