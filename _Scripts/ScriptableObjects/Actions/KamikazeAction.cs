﻿using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Kamikaze")]
public class KamikazeAction : Action
{
    public override void Act(StateController controller)
    {
        Attack(controller);
    }

    private void Attack(StateController controller)
    {
        RaycastHit hit;
        string myTag = controller.transform.tag;
        Debug.DrawRay(controller.eyes.position, controller.eyes.forward.normalized * controller.enemyStats.attackRange, Color.red);

        if (Physics.SphereCast(controller.eyes.position, controller.enemyStats.lookSphereCastRadius, controller.eyes.forward, out hit, controller.enemyStats.attackRange)
            && !hit.collider.CompareTag(myTag))
        {
            if (controller.targetToChase != null)
            {
                controller.kamikaze.targetObjDestination = controller.targetToChase.gameObject;
            }
        }
    }
}