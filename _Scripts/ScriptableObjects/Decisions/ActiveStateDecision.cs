﻿using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/ActiveState")]
public class ActiveStateDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        if (controller.targetToChase != null)
        {
            bool targetToChaseIsActive = controller.targetToChase.gameObject.activeSelf;
            return targetToChaseIsActive;
        }
        return false;
    }
}