﻿using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/Look")]
public class LookDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        bool targetVisible = Look(controller);
        return targetVisible;
    }

    private bool Look(StateController controller)
    {
        string myTag = controller.transform.tag;
        RaycastHit hit;

        Debug.DrawRay(controller.eyes.position, controller.eyes.forward.normalized * controller.enemyStats.lookRange, Color.green);
        if (myTag == "GreenTeam")
        {
            if (Physics.SphereCast(controller.eyes.position, controller.enemyStats.lookSphereCastRadius, controller.eyes.forward, out hit, controller.enemyStats.lookRange)
                         && hit.collider.CompareTag("RedTeam"))
            {
                controller.targetToChase = hit.transform;
                return true;
            }
            else
            {
                controller.navMeshAgent.stoppingDistance = 10f;
                return false;
            }
        }
        if (myTag == "RedTeam")
        {
            if (Physics.SphereCast(controller.eyes.position, controller.enemyStats.lookSphereCastRadius, controller.eyes.forward, out hit, controller.enemyStats.lookRange)
                        && hit.collider.CompareTag("GreenTeam"))
            {
                controller.targetToChase = hit.transform;
                return true;
            }
            else
            {
                controller.navMeshAgent.stoppingDistance = 10f;
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}